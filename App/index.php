<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Decorator</title>
</head>
<body>
<?php

use PatternDecorator\App\boissons\Expresso;
use PatternDecorator\App\decorator\Caramel;
use PatternDecorator\App\decorator\Chocolat;

require_once (dirname(__DIR__). "/vendor/autoload.php");


//I create expresso
    $boisson = new Expresso();
    echo "__________________________Expresso_______________________<br>";
    echo $boisson->getDescription()," ". $boisson->cout() ." € <br>";
    echo "_____________________________________________________________<br>";
    $boisson = new Chocolat($boisson);
    echo $boisson->getDescription()," ". $boisson->cout() ." € <br>";
    echo "_____________________________________________________________<br>";
    $boisson = new Caramel($boisson);
    echo $boisson->getDescription()," ". $boisson->cout() ." € <br>";
    echo "_____________________________________________________________<br>";
    $boisson = new Chocolat($boisson);
    echo $boisson->getDescription()," ". $boisson->cout() ." € <br>";

?>
</body>
</html>
