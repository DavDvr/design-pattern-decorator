<?php

namespace PatternDecorator\App\decorator;
use PatternDecorator\App\boissons\Boisson;

/**
 * Decorator Caramel
 * Class Caramel
 * @package PatternDecorator\App\classAbstractDecorator
 */
class Caramel extends AbstractDecorator
{

    /**
     * Caramel constructor.
     * @param Boisson $boisson
     */
    public function __construct(Boisson $boisson)
    {
        parent::__construct($boisson);
    }

    /**
     * @return float the price of the topping caramel with the price of the drink
     */
    public function cout(): float
    {
        return 0.95 + $this->boisson->cout();
    }

    /**
     * @return string describe the drink
     */
    public function getDescription(): string
    {
        return $this->boisson->getDescription() . " avec du caramel";
    }
}