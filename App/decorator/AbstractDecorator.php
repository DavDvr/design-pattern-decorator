<?php

namespace PatternDecorator\App\decorator;

use PatternDecorator\App\boissons\Boisson;

/**
 * Class AbstractDecorator
 * @package PatternDecorator\App\classAbstractDecorator
 */
abstract class AbstractDecorator extends Boisson
{
    protected Boisson $boisson;

    /**
     * AbstractDecorator constructor.
     * @param Boisson $boisson
     */
    public function __construct(Boisson $boisson)
    {
        $this->boisson = $boisson;

    }

    public function getDescription(): string
    {
        return parent::getDescription();
    }


}