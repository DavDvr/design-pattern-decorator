<?php

namespace PatternDecorator\App\decorator;
use PatternDecorator\App\boissons\Boisson;

/**
 * Decorator Vanille
 * Class Vanille
 * @package PatternDecorator\App\classAbstractDecorator
 */
class Vanille extends AbstractDecorator
{
    /**
     * Vanille constructor.
     * @param Boisson $boisson
     */
    public function __construct(Boisson $boisson)
    {
        parent::__construct($boisson);
    }

    /**
     * @return float price topping vanille with price boisson
     */
    public function cout(): float
    {
        return 0.9 + $this->boisson->cout();
    }

    /**
     * @return string describe the drink
     */
    public function getDescription(): string
    {
        return $this->boisson->getDescription(). "avec de la vanille";
    }
}