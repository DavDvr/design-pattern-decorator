<?php

namespace PatternDecorator\App\decorator;

use PatternDecorator\App\boissons\Boisson;

/**
 * Decorator chocolat
 * Class Chocolat
 * @package PatternDecorator\App\classAbstractDecorator
 */
class Chocolat extends AbstractDecorator
{
    /**
     * Chocolat constructor.
     * @param Boisson $boisson
     */
    public function __construct(Boisson $boisson)
    {
       parent::__construct($boisson);
    }

    /**
     * @return float price topping chocolat with price boisson
     */
    public function cout(): float
    {
        return 0.7 + $this->boisson->cout();
    }

    /**
     * @return string describe the drink
     */
    public function getDescription(): string
    {
        return $this->boisson->getDescription(). " avec du chocolat";
    }
}