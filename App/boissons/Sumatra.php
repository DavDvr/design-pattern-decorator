<?php


namespace PatternDecorator\App\boissons;

class Sumatra extends Boisson
{

    /**
     * Sumatra constructor.
     */
    public function __construct()
    {
        $this->description = "Sumatra";
    }

    public function cout(): float
    {
        return 3.50;
    }
}