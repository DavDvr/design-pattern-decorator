<?php

namespace PatternDecorator\App\boissons;

abstract class Boisson
{
    protected string $description;

    public abstract function cout(): float;

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}