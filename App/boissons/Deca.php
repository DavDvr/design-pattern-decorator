<?php


namespace PatternDecorator\App\boissons;

/**
 * Class Deca
 * @package PatternDecorator\App\boissons
 */
class Deca extends Boisson
{

    /**
     * Deca constructor.
     */
    public function __construct()
    {
        $this->description = "Deca";
    }

    public function cout(): float
    {
        return 2.50;
    }
}