<?php

namespace PatternDecorator\App\boissons;

class Expresso extends Boisson
{

    public function __construct()
    {
        $this->description = "Expresso";
    }

    public function cout(): float
    {
        return 3.00;
    }
}